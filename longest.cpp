#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

int main(void) {

	std::ifstream	infile("input.txt");
	std::string	line;
	int		longest = 0;
	int		tmp = 0;
	
	std::getline(infile, line);
	for(int i = 0; i < line.size(); i++) {
		if (line[i] == '1') {
			tmp++;
			if (tmp > longest)
				longest = tmp;
		} else {
			tmp = 0;
		}
	}

	std::ofstream	myfile;
	myfile.open("output.txt");
	myfile << longest << std::endl;
	myfile.close();
}
